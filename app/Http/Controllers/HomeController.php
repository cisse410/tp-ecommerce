<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

class HomeController extends Controller
{
    public function home(){
        $categories = Category::all();
        $last_product = Product::orderBy('id', 'desc')->limit(3)->get();
        return view('home', compact('categories', 'last_product'));
    }
}

<div class="form-floating mb-3">
    <input value="@isset($product->title){{ $product->title }}@endisset" type="text" name="title"
        id="title" class="form-control" placeholder="Titre du produit">
    <label for="title">Titre du produit</label>
    @error('title')
        <div class="">{{ $message }} </div>
    @enderror
</div>

<div class="form-floating mb-3">
    <input value="@isset($product->price){{ $product->price }}@endisset" type="number" name="price"
        id="price" class="form-control" placeholder="Prix du produit">
    <label for="price">Prix du produit</label>
    @error('price')
        <div class="">{{ $message }} </div>
    @enderror
</div>

<div class="form-floating">
    <textarea name="description" id="description" class="form-control">
@isset($product->description)
{{ $product->description }}
@endisset
</textarea>
    <label for="description">Description du produit</label>
</div>

<div class="form-floating mb-3">
    <input value="@isset($product->image){{ $product->image }}@endisset" type="text" name="image"
        id="image" class="form-control" placeholder="Image du produit">
    <label for="image">Image du produit</label>
    @error('image')
        <div class="">{{ $message }} </div>
    @enderror
</div>

<div class="form-floating mb-3">
    <input value="@isset($product->stock){{ $product->stock }}@endisset" type="number" name="stock"
        id="stock" class="form-control" placeholder="Stock du produit">
    <label for="stock">Stock du produit</label>
</div>

<div class="form-floating mb-3">

    <select name="category_id" id="category_id" class="form-select">

        @if (isset($product->category_id))
            <option value="{{ $product->category_id }}">
                {{ $product->category->name }}
            </option>
        @else
            <option checked>
                Choisisser la catégorie du produit
            </option>
        @endif


        @foreach ($categories as $category)
            @if (isset($product->category_id))
                @if ($product->category_id != $category->id)
                <option value="{{ $category->id }}">
                    {{ $category->name }}
                </option>
                @endif
                @else
                <option value="{{ $category->id }}">
                    {{ $category->name }}
                </option>
            @endif
            
        @endforeach
    </select>
    @error('category_id')
        <div class="">{{ $message }} </div>
    @enderror
</div>

<div class="form-check">
    <input id="status" type="checkbox" value="1" class="form-check-inoput" checked>
    <label for="status" class="form-check-label">
        Statut du produit
    </label>
</div>

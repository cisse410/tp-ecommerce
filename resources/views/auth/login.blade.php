@extends('layout')
@section('title', 'Connexion')
@section('content')
    <div class="row mt-5 justify-content-center">
        <div class="col md-8">
            <div class="card">
                <div class="card-header text-center">SE CONNECTER</div>
                <div class="card-body">
                    <form action="" method="POST">
                        @csrf
                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end text-start">
                                Votre email
                            </label>
                                <div class="col-md-6">
                                    <input type="email" name="email" id="email" class="form-control" @error('email') is-inavalid @enderror value="{{ @old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="text-danger">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                                </div>
                        </div>
                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end text-start">
                                Mot de passe
                            </label>
                                <div class="col-md-6">
                                    <input type="password" name="password" id="password" class="form-control" @error('password') is-inavalid @enderror>
                                    @if ($errors->has('password'))
                                        <span class="text-danger">
                                            {{ $errors->first('password') }}
                                        </span>
                                    @endif
                                </div>
                            </div>

                        <div class="row mb-3">
                            <input type="submit" value="Se connecter" class="col-md-3 offset-md-5 btn btn-primary">
                            <small class="text-center mt-3">Je suis nouveau ici ? <a href="{{ route('register') }}" class="fw-bold">S'inscrire</a></small>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

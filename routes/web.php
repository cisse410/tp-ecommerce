<?php


use App\Http\Controllers\DemoController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginRegisterController;

use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
/*
Route::get('/{nom}', function($n){
    Return "Bonjour <strong>$n</strong>";
});*/
Route::get('/', [HomeController::class, 'home'])->name('home');

Route::prefix('admin')->group(function(){
    Route::resource("categories", CategoryController::class)
                                    ->except(['show'])
                                    ->names('categories');

    Route::resource("products", ProductController::class)->names("products");
});


Route::get('/demo/{id}',[DemoController::class, "voir"])->name("demo");
Route::get("about",[DemoController::class, "about"])->name('about');
Route::controller(LoginRegisterController::class)->group(function(){
    Route::get('/register', 'register')->name('register');
    Route::post('/register', 'userStore')->name('userStore');
    Route::get('/dashboard', 'dashboard')->name('dashboard');
    Route::get('/login', 'login')->name('login');
    Route::post('/login', 'authenticate')->name('authenticate');
    Route::get('/logout', 'logout')->name('logout');
});

<nav class="navbar navbar-expand-lg bg-primary" data-bs-theme="dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{ route('home') }}">LGI Commerce</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="/">Accueil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Administration
                </a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="{{ route('categories.index') }}">Gestion des catégories</a></li>
                    <li><a class="dropdown-item" href="{{ route('products.index') }}">Gestion des produits</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="#">Tableau de bord</a></li>
                </ul>
            </li>
        </ul>
        <form class="d-flex" role="search">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            {{-- <button class="btn btn-outline-success" type="submit">Search</button> --}}
        </form>
        {{-- <div class="d-grid gap-2 d-md-block">
            <a href="{{ route('login') }}" class="btn btn-primary" type="button">Log In</a>
            <a href="{{ route('register') }}" class="btn btn-primary active" type="button">Register</a>
        </div> --}}
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            @guest
                <a href="{{ route('login') }}" class="btn btn-primary" type="button">Se connecter</a>
                @else
                <form class="d-flex" action="{{ route('logout') }}" method="get">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-sm btn-danger">Se deconnecter</button>
                </form>
            @endguest
        </div>
      </div>
    </div>
  </nav>

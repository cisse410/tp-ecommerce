@extends('layout')
@section('title', 'Inscription')
@section('content')
    <div class="row mt-5 justify-content-center">
        <div class="col md-8">
            <div class="card">
                <div class="card-header text-center">S'INSCRIRE</div>
                <div class="card-body">
                    <form action="" method="POST">
                        @csrf
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end text-start">
                                Votre nom
                            </label>
                                <div class="col-md-6">
                                    <input type="text" name="name" id="name" class="form-control" @error('name') is-inavalid @enderror value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                        <span class="text-danger">
                                            {{ $errors->first('name') }}
                                        </span>
                                    @endif
                                </div>
                        </div>
                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end text-start">
                                Votre email
                            </label>
                                <div class="col-md-6">
                                    <input type="email" name="email" id="email" class="form-control" @error('email') is-inavalid @enderror value="{{ @old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="text-danger">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                                </div>
                        </div>
                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end text-start">
                                Mot de passe
                            </label>
                                <div class="col-md-6">
                                    <input type="password" name="password" id="password" class="form-control" @error('password') is-inavalid @enderror>
                                    @if ($errors->has('password'))
                                        <span class="text-danger">
                                            {{ $errors->first('password') }}
                                        </span>
                                    @endif
                                </div>
                        </div>
                        <div class="row mb-3">
                            <label for="password_confirmation" class="col-md-4 col-form-label text-md-end text-start">
                                Confirmer le mot de passe
                            </label>
                                <div class="col-md-6">
                                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" @error('password_confirmation') is-inavalid @enderror>
                                    @if ($errors->has('password'))
                                        <span class="text-danger">
                                            {{ $errors->first('password_confirmation') }}
                                        </span>
                                    @endif
                                </div>
                        </div>

                        <div class="row mb-3">
                            <input type="submit" value="S'inscrire" class="col-md-3 offset-md-5 btn btn-primary">
                            <small class="text-center mt-3">J'ai deja un compte, <a href="{{ route('login') }}" class="fw-bold">Se connecter</a></small>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

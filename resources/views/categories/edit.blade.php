@extends('layout')

@section('title', "Modification d'une catégorie")

@section('content')
    <h3>Modification de la catégorie : 
        <strong>{{ $category->name }}</strong>
    </h3>
    <form action="{{ route('categories.update', $category->id) }}" 
        method="post">

        @csrf
        @method('PUT')
        @include('categories.form')
        
        <div class="d-grid gap-2">
            <button type="submit" class="btn btn-warning">
                Modifier la catégorie
            </button>
            <a href="{{ route('categories.index') }}" 
                class="btn btn-secondary">
                Annuler la modification
            </a>
        </div>
    </form>
@endsection
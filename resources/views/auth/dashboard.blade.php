@extends('layout')
@section('title', 'Tableau de board')
@section('content')
    <div class="row justify-content-center mt-5">
        <div class="card">
            <div class="card-header">Tableau de board</div>
            <div class="card-body">
                @if (Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                    @else
                        <div class="alert alert-success">
                            Vous etes conectes ! <br>
                            Bienvenue {{ Auth::user()->name }}
                        </div>
                @endif
            </div>
        </div>
    </div>
@endsection
